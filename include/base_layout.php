<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ระบบสำรองที่นั่งโรงภาพยนตร์</title>
    <link rel="stylesheet" href="<?= url('/assets/css/utility.css') ?>">
    <link rel="stylesheet" href="<?= url('/assets/css/style.css') ?>">
    <?= isset($layout_head) ? $layout_head : null ?>
</head>
<body>
    <?= isset($layout_body) ? $layout_body : null ?>
</body>
</html>