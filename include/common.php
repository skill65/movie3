<?php

function conf($key) {
    global $config;
    return isset($config[$key]) ? $config[$key] : null;
}

function url($path = '') {
    return SITE_URL . $path;
}

function redirect($path = '') {
    header('Location: ' . url($path));
    exit;
}

function setFlash($key, $val) {
    $_SESSION['flash'][$key] = $val;
}

function getFlash($key, $unset = false) {
    global $flash;
    if (isset($flash[$key])) {
        return $flash[$key];
    } elseif (isset($_SESSION['flash'][$key])) {
        $val = $_SESSION['flash'][$key];
        if ($unset) {
            unset($_SESSION['flash'][$key]);
        }
        return $val;
    } 
    return null;
}

function setAlert($status, $message) {
    setFlash('alert', [
        'status' => $status,
        'message' => $message
    ]);
}

function showAlert() {
    $alert = getFlash('alert', true);
    if (empty($alert))
        return;
    
    $status =  isset($alert['status']) ? $alert['status'] : null;
    $message =  isset($alert['message']) ? $alert['message'] : null;
    switch ($status) {
        case 'error':
            ?>
            <div class="alert alert-red mb-3">
                <?= $message ?>
            </div>
            <?php
            break;

        case 'success':
            ?>
            <div class="alert alert-green mb-3">
                <?= $message ?>
            </div>
            <?php
            break;
        
        default:
            ?>
            <div class="alert mb-3">
                <?= $message ?>
            </div>
            <?php
            break;
    }
}

function upload($input, $dir = '/storage') {
    if (empty($_FILES[$input]['name']))
        return false; 

    $path = $dir . '/';
    $path .= uniqid();
    $path .= '.';
    $path .= pathinfo($_FILES[$input]['name'], PATHINFO_EXTENSION);
    if (move_uploaded_file($_FILES[$input]['tmp_name'], ROOT . $path));
        return $path;

    return false; 
}

function clickConfirm($message) {
    return "onclick=\"return confirm('{$message}')\"";
}

function checkLogin() {
    global $db, $user, $user_id;
    if (empty($_SESSION['user_id'])) {
        redirect('/auth/login.php');
    }
    
    $user_id = $_SESSION['user_id'];
    $result_user = $db->query("SELECT * FROM `users` WHERE `user_id`='{$user_id}'");
    $user = $result_user->fetch_assoc();
    
    if (empty($user) || $user['status'] !== '1') {
        redirect('/auth/login.php');
    }
}

function checkAuth($user_type) {
    global $user;
    checkLogin();

    if (empty($user) || $user['user_type'] !== $user_type) {
        redirect('/auth/login.php');
    }
}

function fetchAll($result) {
    $data = [];
    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }
    return $data;
}

function get($key) {
    return isset($_GET[$key]) ? $_GET[$key] : null;
}

function post($key) {
    return isset($_POST[$key]) ? $_POST[$key] : null;
}
