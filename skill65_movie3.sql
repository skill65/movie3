CREATE TABLE `users` (
  `user_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสผู้ใช้',
  `firstname` varchar(25) NOT NULL COMMENT 'ชื่อ',
  `lastname` varchar(25) NOT NULL COMMENT 'นามสกุล',
  `profile` varchar(100) COMMENT 'ภาพประจำตัว',
  `email` varchar(100) UNIQUE NOT NULL COMMENT 'อีเมล',
  `password` varchar(64) NOT NULL COMMENT 'รหัสผ่าน',
  `user_type` ENUM ('admin', 'user') NOT NULL COMMENT 'ประเภทบัญชี',
  `status` int(1) NOT NULL COMMENT '-1 = ระงับการใช้งาน
0 = ขอใช้งานระบบ
1 = ใช้งานระบบ'
);

CREATE TABLE `movies` (
  `movie_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสภาพยนตร์',
  `name` varchar(50) NOT NULL COMMENT 'ชื่อภาพยนตร์',
  `poster` varchar(100) COMMENT 'ภาพโปสเตอร์ภาพยนตร์'
);

CREATE TABLE `movie_times` (
  `movie_time_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสเวลาฉายภาพยนตร์',
  `movie_id` int(11) NOT NULL COMMENT 'รหัสภาพยนตร์',
  `start_time` datetime NOT NULL COMMENT 'วันเวลาเริ่มฉายภาพยนตร์',
  `end_time` datetime NOT NULL COMMENT 'วันเวลาสิ่นสุดการฉายภาพยนตร์'
);

CREATE TABLE `theater_seats` (
  `theater_seat_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสที่นั่งโรงภาพยนตร์',
  `seat_name` varchar(5) NOT NULL COMMENT 'ชื่อที่นั่งโรงภาพยนตร์'
);

CREATE TABLE `theater_plan` (
  `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสผังที่นั่งโรงภาพยนตร์',
  `img` varchar(100) NOT NULL COMMENT 'ภาพผังที่นั่งโรงภาพยนตร์'
);

CREATE TABLE `reserve_action` (
  `reserve_action_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสการจองที่นั่ง',
  `user_id` int(11) NOT NULL COMMENT 'รหัสผู้ใช้',
  `movie_time_id` int(11) NOT NULL COMMENT 'รหัสเวลาฉายภาพยนตร์',
  `status` int(1) NOT NULL COMMENT '-1 = ปฏิเสธการจอง
0 = รอการอนุมัติการจอง
1 = อนุมัติการจองแล้ว'
);

CREATE TABLE `reserve_items` (
  `reserve_item_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'รหัสรายการที่นั่งที่จอง',
  `reserve_action_id` int(11) NOT NULL COMMENT 'รหัสการจองที่นั่ง',
  `theater_seat_id` int(11) NOT NULL COMMENT 'รหัสที่นั่งโรงภาพยนตร์'
);

ALTER TABLE `users` COMMENT = '# users 
ตารางผู้ใช้
## Example table
| user_id | firstname | lastname | profile | email | password | user_type | status |
| ----: | ---- | ---- | ---- | ---- | ---- | ---- | ----: | 
| 1 | admin | demo | /storage/profiles/5ece4797eaf5e.jpg | admin@demo.com | 25d55ad283aa400af464c76d713c07ad | admin | 1 |
| 2 | user | demo | /storage/profiles/63a936d4a716d.jpg | user@demo.com | 25d55ad283aa400af464c76d713c07ad | user | 1 |
| 3 | user1 | demo | /assets/img/profile.png | user1@demo.com | 25d55ad283aa400af464c76d713c07ad | user | 0 |
| 4 | user2 | demo | /assets/img/profile.png | user2@demo.com | 25d55ad283aa400af464c76d713c07ad | user | -1 |';

ALTER TABLE `movies` COMMENT = '# movies
ตารางภาพยนตร์
## Example table
| movie_id | name | poster |
| ----: | ---- | ---- |
| 1 | Avatar: The Way of Water | /storage/posters/63a93871328d1.jpg |
| 2 | Wednesday | /storage/posters/63a938a7d1d92.jpg |
| 3 | WALL·E | /storage/posters/63a938ad2c0c0.jpg |';

ALTER TABLE `movie_times` COMMENT = '# movie_times
ตารางเวลาฉายภาพยนตร์
## Example table
| movie_time_id | movie_id | start_time | end_time |
| ----: | ---- | ---- | ---- |
| 1 | 1 | 2022-1-20 13:30:00 | 2022-1-20 15:30:00 |
| 2 | 1 | 2022-1-20 17:00:00 | 2022-1-20 19:00:00 |
| 3 | 2 | 2022-1-21 9:00:00 | 2022-1-21 11:00:00 |';

ALTER TABLE `theater_seats` COMMENT = '# theater_seats
ตารางที่นั่งโรงภาพยนตร์
## Example table
| theater_seat_id | seat_name |
| ----: | ---- |
| 1 | A1 | 
| 2 | A2 | 
| 3 | A3 | 
| 4 | B1 | 
| 5 | B2 |
| 6 | C1 | ';

ALTER TABLE `theater_plan` COMMENT = '# theater_plan
ตารางผังที่นั่งโรงภาพยนตร์
## Example table
| id | img |
| ----: | ---- |
| 1 | /storage/plan/63a938bace23a.jpg | ';

ALTER TABLE `reserve_action` COMMENT = '# reserve_action
ตารางการจองที่นั่งโรงภาพยนตร์
## Example table
| reserve_action_id | user_id | movie_time_id | status |
| ----: | ----: | ----: | ----: |
| 1 | 2 | 1 | 1 |
| 2 | 2 | 2 | 0 |
| 3 | 4 | 3 | -1 |';

ALTER TABLE `reserve_items` COMMENT = '# reserve_items
ตารางรายการที่นั่งที่จอง
## Example table
| reserve_item_id | reserve_action_id | theater_seat_id |
| ----: | ----: | ----: |
| 1 | 1 | 1 |
| 2 | 1 | 2 |
| 3 | 1 | 3 |
| 4 | 2 | 1 |
| 5 | 3 | 3 |';

ALTER TABLE `movie_times` ADD FOREIGN KEY (`movie_id`) REFERENCES `movies` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `reserve_action` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `reserve_action` ADD FOREIGN KEY (`movie_time_id`) REFERENCES `movie_times` (`movie_time_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `reserve_items` ADD FOREIGN KEY (`reserve_action_id`) REFERENCES `reserve_action` (`reserve_action_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `reserve_items` ADD FOREIGN KEY (`theater_seat_id`) REFERENCES `theater_seats` (`theater_seat_id`) ON DELETE CASCADE ON UPDATE CASCADE;


INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `profile`, `email`, `password`, `user_type`, `status`) VALUES
(1, 'admin', 'demo', '/storage/profiles/63aaa25900341.jpg', 'admin@demo.com', '25d55ad283aa400af464c76d713c07ad', 'admin', 1),
(2, 'user', 'demo', '/storage/profiles/63abbb7f9993a.jpg', 'user@demo.com', '25d55ad283aa400af464c76d713c07ad', 'user', 1),
