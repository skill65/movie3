<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$search = get('search');
$search = trim($search);

$reslut = $db->query("SELECT * FROM `movies` WHERE `name` LIKE '%{$search}%'");
$items = fetchAll($reslut);
ob_start();
?>

<div class="card mb-5">
    <div class="card-body">
        <form method="get">
            <label for="search">ค้นหาภาพยนตร์</label>
            <input type="search" name="search" id="search" value="<?= $search ?>">
            <button type="submit" class="btn btn-main mt-3">ค้นหา</button>

            <!-- <div class="row">
                <div class="col" style="width: inherit;">
                    <label for="search">ค้นหาภาพยนตร์</label> 
                </div>
                <div class="col"> 
                    <input type="search" name="search" id="search">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-main">ค้นหา</button>
                </div>
            </div> -->
        </form>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <?php foreach ($items as $item) : ?>
                <div class="col-3">
                    <div class="card">
                        <img src="<?= url($item['poster']) ?>" alt="" class="card-img">
                        <div class="card-body">
                            <div class="card-title">
                                <?= $item['name'] ?>
                            </div>
                            <i>รหัส: <?= $item['movie_id'] ?></i>
                            <a href="<?= url("/user/movies/detail.php?movie_id={$item['movie_id']}") ?>" class="btn btn-main btn-sm mt-2 w-100">
                                รายละเอียด
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'ค้นหาภาพยนตร์';
include ROOT . '/user/layout.php';
