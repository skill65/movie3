<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$movie_id = get('movie_id');

if (empty($movie_id)) {
    redirect('/user/movies/search.php');
}
$page_path = "/user/movies/detail.php?movie_id=$movie_id";

if (!empty($_POST)) {
    if (empty($_POST['seats'])) {
        setAlert('error', 'กรุณาเลือกที่นั่ง');
        redirect($page_path);
    }

    $qr = $db->query("INSERT INTO `reserve_action`(
    `user_id`, 
    `movie_time_id`, 
    `status`) VALUES (
    '{$user_id}',
    '{$_POST['movie_time_id']}',
    0)");
    if (!$qr) {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถจองที่นั่งโรงภาพยนตร์ได้');
        redirect($page_path);
    }

    $reserve_action_id = $db->insert_id;

    $seats = [];
    foreach ($_POST['seats'] as $seat) {
        $seats[] = "('{$reserve_action_id}','{$seat}')";
    }
    $seats_sql = implode(',', $seats);
    $qr_items = $db->query("INSERT INTO `reserve_items`(
    `reserve_action_id`, 
    `theater_seat_id`) VALUES {$seats_sql}");

    if ($qr_items) {
        setAlert('success', "จองที่นั่งโรงภาพยนตร์สำเร็จ");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถจองที่นั่งได้");
    }
    redirect($page_path);
}

$re = $db->query("SELECT * FROM `movies` WHERE `movie_id`='{$movie_id}'");
$data = $re->fetch_assoc();

$re_items = $db->query("SELECT * FROM `movie_times` WHERE `movie_id`='{$movie_id}'");
$items = fetchAll($re_items);

$re_items2 = $db->query("SELECT * FROM `theater_seats`");
$items2 = fetchAll($re_items2);

$re_plan = $db->query("SELECT * FROM `theater_plan` ORDER BY `id` DESC");
$plan = $re_plan->fetch_assoc();
ob_start();
?>

<div class="card">
    <div class="card-body">
        <?php showAlert() ?>
        <div class="text-center">
            <img src="<?= url($data['poster']) ?>" alt="" class="mw-21r mh-21r">
        </div>
        <p class="text-center">
            (<?= $data['movie_id'] ?>) <?= $data['name'] ?>
        </p>

        <form method="post">
            <table class="mt-3">
                <thead>
                    <tr>
                        <th>รหัสวันเวลาฉาย</th>
                        <th>วันเวลาเริ่มฉายภาพยนตร์</th>
                        <th>วันเวลาจบการฉายภาพยนตร์</th>
                        <th>เลือกเวลาฉาย</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($items as $item) : ?>
                        <tr>
                            <td><?= $item['movie_time_id'] ?></td>
                            <td><?= $item['start_time'] ?></td>
                            <td><?= $item['end_time'] ?></td>
                            <td>
                                <input type="radio" name="movie_time_id" id="movie_time_id" value="<?= $item['movie_time_id'] ?>" style="transform: scale(1.5);" required>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div class="text-center mt-4">
                <img src="<?= url($plan['img']) ?>" alt="" class="mw55r mh-21r">
            </div>
            <h4 class="mt-3">เลือกที่นั่งโรงภาพยนตร์</h4>
            <?php foreach ($items2 as $item) : ?>
                <input type="checkbox" name="seats[]" id="seats<?= $item['theater_seat_id'] ?>" value="<?= $item['theater_seat_id'] ?>" style="transform: scale(1.5);">
                <label for="seats<?= $item['theater_seat_id'] ?>" class="mr-5"><?= $item['seat_name'] ?></label>
            <?php endforeach; ?>
            <div class="mt-3">
                <button type="submit" class="btn btn-main">จองที่นั่งโรงภาพยนตร์</button>
            </div>
        </form>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "รายละเอียดภาพตนตร์";
require ROOT . '/user/layout.php';
