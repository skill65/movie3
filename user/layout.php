<?php
ob_start();
?>
<div id="layout">
    <nav id="menu">
        <div class="head top-bar">
            ระบบสำรองที่นั่งโรงภาพยนตร์
        </div>
        <div class="menu-list scroll">
            <a href="<?= url('/user/index.php') ?>" class="menu-item <?= $page_name === 'หน้าหลัก' ? 'active' : '' ?>">หน้าหลัก</a>

            <span class="menu-title">จองที่นั่งโรงภาพยนตร์</span>
            <a href="<?= url('/user/movies/search.php') ?>" class="menu-item <?= $page_name === 'ค้นหาภาพยนตร์' ? 'active' : '' ?>">ค้นหาภาพยนตร์</a>

            <span class="menu-title">ข้อมูลส่วนตัว</span>
            <a href="<?= url('/user/profile/edit.php') ?>" class="menu-item <?= $page_name === 'แก้ไขข้อมูลส่วนตัว' ? 'active' : '' ?>">แก้ไขข้อมูลส่วนตัว</a>
            <a href="<?= url('/user/profile/edit-pass.php') ?>" class="menu-item <?= $page_name === 'แก้ไขรหัสผ่าน' ? 'active' : '' ?>">แก้ไขรหัสผ่าน</a>
            <a href="<?= url('/auth/logout.php') ?>" <?= clickConfirm('คุณต้องการออกจากระบบหรือไม่') ?> class="menu-item">ออกจากระบบ</a>
            <br><br><br>
        </div>
    </nav>

    <header class="top-bar">
        <div><?= isset($page_name) ? $page_name : '' ?></div>
        <div class="avatar">
            <img src="<?= url($user['profile']) ?>" alt="">
            <span class="name"><?= $user['firstname'] . ' ' . $user['lastname'] ?></span>
            <span> <i>(ผู้ใช้งานระบบ)</i></span>
        </div>
    </header>

    <main>
        <?= isset($layout_page) ? $layout_page : '' ?>
        <footer class="">
        </footer>
    </main>
</div>

<?php
$layout_body = ob_get_clean();
$layout_head = '<link rel="stylesheet" href="' . url('/assets/css/layout.css') . '">';
require INC . '/base_layout.php';
