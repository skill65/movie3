<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = '/user/profile/edit.php';

if (!empty($_POST)) {
    if (!empty($_FILES['profile']['name'])) {
        $profile = upload('profile', '/storage/profiles');
        if ($profile === false) {
            setAlert('error', 'เกิดข้อผิดพลาด ไม่สามาอัพโหลดภาพได้');
            redirect($page_path);
        }

        $qr_profile = $db->query("UPDATE `users` SET 
        `profile`='$profile'
        WHERE `user_id`='$user_id'");
    }

    $result_email = $db->query("SELECT * FROM `users` WHERE `email`='{$_POST['email']}' AND `user_id`!='$user_id'");

    if ($result_email->num_rows !== 0) {
        setAlert('error', "มีอีเมล {$_POST['email']} แล้วไม่สามารถแก้ไขซ้ำได้");
        redirect($page_path);
    }

    $qr = $db->query("UPDATE `users` SET 
    `firstname`='{$_POST['firstname']}',
    `lastname`='{$_POST['lastname']}',
    `email`='{$_POST['email']}'
    WHERE `user_id`='$user_id'");

    if ($qr) {
        setAlert('success', 'แก้ไขข้อมูลส่วนตัวสำเร็จเรียบร้อย');
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลส่วนตัวได้');
    }
    redirect($page_path);
}

ob_start();
?>
<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post" enctype="multipart/form-data">
            <div class="text-center">
                <label for="profile">
                    <img src="<?= url($user['profile']) ?>" alt="" class="circle w-14r h-14r">
                </label>
            </div>
            <label for="profile">ภาพประจำตัว</label>
            <input type="file" name="profile" id="profile" class="mb-3" accept="image/*">
            <label for="firstname">ชื่อ</label>
            <input type="text" name="firstname" id="firstname" class="mb-3" value="<?= $user['firstname'] ?>" required>
            <label for="lastname">นามสกุล</label>
            <input type="text" name="lastname" id="lastname" class="mb-3" value="<?= $user['lastname'] ?>" required>
            <label for="email">อีเมล</label>
            <input type="email" name="email" id="email" class="mb-3" value="<?= $user['email'] ?>" required>

            <div class="text-center mt-3">
                <button type="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขข้อมูลส่วนตัว';

include ROOT . '/user/layout.php';
