<?php

require_once __DIR__ . '/../boot.php';
checkAuth('user');

$reslut = $db->query("SELECT * FROM `movies`");
$items = fetchAll($reslut);
ob_start();
?>

<h1>ยินดีต้อนรับ <?= $user['firstname'] . ' ' . $user['lastname'] ?></h1>

<div class="card">
    <div class="card-body">
        <div class="row">
            <?php foreach ($items as $item) : ?>
                <div class="col-3">
                    <div class="card">
                        <img src="<?= url($item['poster']) ?>" alt="" class="card-img">
                        <div class="card-body">
                            <div class="card-title">
                                <?= $item['name'] ?>
                            </div>
                            <i>รหัส: <?= $item['movie_id'] ?></i>
                            <a href="<?= url("/user/movies/detail.php?movie_id={$item['movie_id']}") ?>" class="btn btn-main btn-sm mt-2 w-100">
                                รายละเอียด
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'หน้าหลัก';
include ROOT . '/user/layout.php';