<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movie-times/list.php';

$action = get('action');
$id = get('id');
switch ($action) {
    case 'delete':
        $qr = $db->query("DELETE FROM `movie_times` WHERE `movie_time_id`='$id'");
        if ($qr) {
            setAlert('success', 'ลบเวลาฉายภาพยนตร์สำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด  ไม่สามารถลบเวลาฉายภาพยนตร์ได้');
        }
        redirect($page_path);
        break;
}

$re = $db->query("SELECT * FROM `movie_times`
INNER JOIN `movies` ON `movies`.`movie_id`=`movie_times`.`movie_id`");
$items = fetchAll($re);
ob_start();
?>
<div class="card">
    <div class="card-body">
        <?php showAlert() ?>
        <table>
            <thead>
                <tr>
                    <th>รหัส</th>
                    <th>รหัสภาพยนตร์</th>
                    <th>ชื่อภาพยนตร์</th>
                    <th>วันเวลาเริ่มฉายภาพยนตร์</th>
                    <th>วันเวลาจบการฉายภาพยนตร์</th>
                    <th>จัดการเวลาฉาย</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $item['movie_time_id'] ?></td>
                        <td><?= $item['movie_id'] ?></td>
                        <td><?= $item['name'] ?></td>
                        <td><?= $item['start_time'] ?></td>
                        <td><?= $item['end_time'] ?></td>
                        <td><a href="?action=delete&id=<?= $item['movie_time_id'] ?>" class="btn btn-red btn-sm" <?= clickConfirm("คุณต้องการลบเวลาฉายภาพยนตร์หรือไม่") ?>>ลบเวลาฉาย</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการเวลาฉายภาพยนตร์';

include ROOT . '/admin/layout.php';
