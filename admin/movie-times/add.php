<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movie-times/add.php';

if (!empty($_POST)) {
    $movie_id = post('movie_id');
    $db->query("INSERT INTO `movie_times`(
    `movie_id`, 
    `start_time`, 
    `end_time`) 
    VALUES (
    '{$movie_id}',
    '{$_POST['start_time']}',
    '{$_POST['end_time']}')");
}

$re_movies = $db->query("SELECT * FROM `movies`");
$movies = fetchAll($re_movies);
ob_start();
?>

<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post">
            <label for="movie_id">เลือกภาพยนตร์</label>
            <select name="movie_id" id="movie_id" class="mb-3" required>
                <option value="" selected disabled>---- เลือก ----</option>
                <?php foreach ($movies as $movie) : ?>
                    <option value="<?= $movie['movie_id'] ?>">(<?= $movie['movie_id'] ?>) <?= $movie['name'] ?></option>
                <?php endforeach; ?>
            </select>
            <label for="start_time">วันเวลาเริ่มฉายภาพยนตร์</label>
            <input type="datetime-local" name="start_time" id="start_time" class="mb-3" required>
            <label for="end_time">วันเวลาจบการฉายภาพยนตร์</label>
            <input type="datetime-local" name="end_time" id="end_time" class="mb-3" required>


            <div class="text-center mt-3">
                <button type="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "เพิ่มเวลาฉายภาพยนตร์";
require ROOT . '/admin/layout.php';
