<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/theater-seats/add.php';

if (!empty($_POST)) {
    $qr = $db->query("INSERT INTO `theater_seats`(`seat_name`) VALUES ('{$_POST['seat_name']}')");
    if ($qr) {
        setAlert('success', "เพิ่มที่นั่งโรงภาพยนตร์ {$_POST['seat_name']} สำเร็จ");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มที่นั่งโรงภาพยนตร์ {$_POST['seat_name']} ได้");
    }
    redirect($page_path);
}

$re_plan= $db->query("SELECT * FROM `theater_plan` ORDER BY `id` DESC");
$plan = $re_plan->fetch_assoc();
ob_start();
?>

<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post">
            <div class="text-center">
                <img src="<?= url($plan['img']) ?>" alt="" class="mw55r mh-21r">
            </div>
            <label for="seat_name">ชื่อที่นั่งโรงภาพยนตร์</label>
            <input type="text" name="seat_name" id="seat_name" class="mb-3" required>
            <div class="text-center mt-3">
                <button type="submit" name="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "เพิ่มที่นั่งโรงภาพยนตร์";
require ROOT . '/admin/layout.php';
