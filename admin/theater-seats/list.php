<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/theater-seats/list.php';

$action = get('action');
$id = get('id');
switch ($action) {
    case 'delete':
        $qr = $db->query("DELETE FROM `theater_seats` WHERE `theater_seat_id`='$id'");
        if ($qr) {
            setAlert('success', 'ลบที่นั่งโรงภาพยนตร์สำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด  ไม่สามารถลบที่นั่งโรงภาพยนตร์ได้');
        }
        redirect($page_path);
        break;
}

$re = $db->query("SELECT * FROM `theater_seats`");
$items = fetchAll($re);

$re_plan = $db->query("SELECT * FROM `theater_plan` ORDER BY `id` DESC");
$plan = $re_plan->fetch_assoc();
ob_start();
?>
<div class="card">
    <div class="card-body">
        <?php showAlert() ?>
        <div class="text-center">
            <img src="<?= url($plan['img']) ?>" alt="" class="mw55r mh-21r">
        </div>
        <table>
            <thead>
                <tr>
                    <th>รหัส</th>
                    <th>ชื่อที่นั่งโรงภาพยนตร์</th>
                    <th>จัดการที่นั่งโรงภาพยนตร์</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $item['theater_seat_id'] ?></td>
                        <td><?= $item['seat_name'] ?></td>
                        <td><a href="?action=delete&id=<?= $item['theater_seat_id'] ?>" class="btn btn-red btn-sm" <?= clickConfirm("คุณต้องการลบที่นั่งโรงภาพยนตร์หรือไม่") ?>>ลบที่นั่ง</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการที่นั่งโรงภาพยนตร์';

include ROOT . '/admin/layout.php';
