<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/theater-seats/edit-plan.php';

if (!empty($_FILES)) {
    $file = upload('file', '/storage/theater_plan');
    if ($file === false) {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพผังที่นั่งโรงภาพยนตร์ได้');
        redirect($page_path);
    }

    $qr = $db->query("INSERT INTO `theater_plan`(`img`) VALUES ('{$file}')");
    if ($qr) {
        setAlert('success', 'แก้ไขผังที่นั่งโรงภาพยนตร์สำเร็จ');
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแก้ไขผังที่นั่งโรงภาพยนตร์ได้');
    }
    redirect($page_path);
}

$re_data = $db->query("SELECT * FROM `theater_plan` ORDER BY `id` DESC");
$data = $re_data->fetch_assoc();
ob_start();
?>

<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post" enctype="multipart/form-data">
            <div class="text-center">
                <label for="file">
                    <img src="<?= url($data['img']) ?>" alt="" class="mw55r mh-21r">
                </label>
            </div>
            <label for="file">ภาพผังที่นั่งโรงภาพยนตร์</label>
            <input type="file" name="file" id="file" class="mb-3" accept="image/*" required>
            <div class="text-center mt-3">
                <button type="submit" name="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "แก้ไขผังที่นั่งโรงภาพยนตร์";
require ROOT . '/admin/layout.php';
