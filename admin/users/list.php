<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/users/list.php';

$action = get('action');
$id = get('id');
switch ($action) {
    case 'cancel':
        $qr = $db->query("UPDATE `users` SET `status`=-1 WHERE `user_id`='$id'");
        if ($qr) {
            setAlert('success', 'ระงับการใช้งานสำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถระงับการใช้งานได้');
        }
        redirect($page_path);
        break;

    case 'uncancel':
        $qr = $db->query("UPDATE `users` SET `status`=1 WHERE `user_id`='$id'");
        if ($qr) {
            setAlert('success', 'ยกเลิกระงับการใช้งานสำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถยกเลิกระงับการใช้งานได้');
        }
        redirect($page_path);
        break;

    case 'delete':
        $qr = $db->query("DELETE FROM `users` WHERE `user_id`='$id'");
        if ($qr) {
            setAlert('success', 'ลบบัญชีผู้ใช้งานสำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถลบบัญชีผู้ใช้งานได้');
        }
        redirect($page_path);
        break;
}

$re = $db->query("SELECT * FROM `users` WHERE `user_type`='user'");
$items = fetchAll($re);
ob_start();
?>
<div class="card">
    <div class="card-body">
        <?php showAlert() ?>
        <table>
            <thead>
                <tr>
                    <th>ภาพประจำตัว</th>
                    <th>รหัส</th>
                    <th>ชื่อ</th>
                    <th>นามสกุล</th>
                    <th>อีเมล</th>
                    <th>สถานะ</th>
                    <th>จัดการผู้ใช้งาน</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td>
                            <div class="avatar">
                                <img src="<?= url($item['profile']) ?>" alt="">
                            </div>
                        </td>
                        <td><?= $item['user_id'] ?></td>
                        <td><?= $item['firstname'] ?></td>
                        <td><?= $item['lastname'] ?></td>
                        <td><?= $item['email'] ?></td>
                        <td>
                            <?php
                            switch ($item['status']) {
                                case '-1':
                                    echo "<span class=\"t-red\">ระงับการใช้งาน</span>";
                                    break;
                                case '1':
                                    echo "<span class=\"t-green\">ใช้งาน</span>";
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            switch ($item['status']) {
                                case '-1':
                            ?>
                                    <a href="?action=uncancel&id=<?= $item['user_id'] ?>" class="btn btn-main btn-sm" <?= clickConfirm("คุณต้องการยกเลิกระงับการใช้งาน {$item['email']} หรือไม่") ?>>ยกเลิกระงับการใช้งาน</a>
                                    <a href="?action=delete&id=<?= $item['user_id'] ?>" class="btn btn-red btn-sm ml-2" <?= clickConfirm("คุณต้องการลบบัญชี {$item['email']} หรือไม่") ?>>ลบบัญชี</a>
                                <?php
                                    break;
                                case '1':
                                ?>
                                    <a href="?action=cancel&id=<?= $item['user_id'] ?>" class="btn btn-red btn-sm" <?= clickConfirm("คุณต้องการระงับการใช้งาน {$item['email']} หรือไม่") ?>>ระงับการใช้งาน</a>
                            <?php
                                    break;
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'แสดงข้อมูลผู้ใช้งานระบบ';

include ROOT . '/admin/layout.php';
