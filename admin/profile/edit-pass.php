<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/profile/edit-pass.php';

if (!empty($_POST)) {
    if ($user['password'] !== md5($_POST['password'])) {
        setAlert('error', 'รหัสผ่นปัจจุบันไม่ถูกต้อง');
        redirect($page_path);
    }

    $hash = md5($_POST['password_new']);
    $qr = $db->query("UPDATE `users` SET `password`='$hash' WHERE `user_id`='$user_id'");
    if ($qr) {
        setAlert('success', 'แก้ไขรหัสผ่านสำเร็จเรียบร้อย');
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแก้ไขรหัสผ่านได้');
    }
    redirect($page_path);
}

ob_start();
?>
<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post">
            <label for="password">รหัสผ่านปัจจุบัน</label>
            <input type="password" name="password" id="password" class="mb-3" required>
            <label for="password_new">รหัสผ่านใหม่</label>
            <input type="password" name="password_new" id="password_new" class="mb-3" required>
            <div class="text-center mt-3">
                <button type="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขรหัสผ่าน';

include ROOT . '/admin/layout.php';
