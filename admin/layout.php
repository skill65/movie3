<?php
ob_start();
?>
<div id="layout">
    <nav id="menu">
        <div class="head top-bar">
            ระบบสำรองที่นั่งโรงภาพยนตร์
        </div>
        <div class="menu-list scroll">
            <a href="<?= url('/admin/index.php') ?>" class="menu-item <?= $page_name === 'หน้าหลัก' ? 'active' : '' ?>">หน้าหลัก</a>
            <span class="menu-title">จัดการข้อมูลผู้ใช้งานระบบ</span>
            <a href="<?= url('/admin/users/list.php') ?>" class="menu-item <?= $page_name === 'แสดงข้อมูลผู้ใช้งานระบบ' ? 'active' : '' ?>">แสดงข้อมูลผู้ใช้งานระบบ</a>

            <span class="menu-title">จัดการภาพยนตร์</span>
            <a href="<?= url('/admin/movies/add.php') ?>" class="menu-item <?= $page_name === 'เพิ่มภาพยนตร์' ? 'active' : '' ?>">เพิ่มภาพยนตร์</a>
            <a href="<?= url('/admin/movies/list.php') ?>" class="menu-item <?= $page_name === 'รายการภาพยนตร์' ? 'active' : '' ?>">รายการภาพยนตร์</a>

            <span class="menu-title">จัดการเวลาฉายภาพยนตร์</span>
            <a href="<?= url('/admin/movie-times/add.php') ?>" class="menu-item <?= $page_name === 'เพิ่มเวลาฉายภาพยนตร์' ? 'active' : '' ?>">เพิ่มเวลาฉายภาพยนตร์</a>
            <a href="<?= url('/admin/movie-times/list.php') ?>" class="menu-item <?= $page_name === 'รายการเวลาฉายภาพยนตร์' ? 'active' : '' ?>">รายการเวลาฉายภาพยนตร์</a>

            <span class="menu-title">จัดการที่นั่งโรงภาพยนตร์</span>
            <a href="<?= url('/admin/theater-seats/edit-plan.php') ?>" class="menu-item <?= $page_name === 'แก้ไขผังที่นั่งโรงภาพยนตร์' ? 'active' : '' ?>">แก้ไขผังที่นั่งโรงภาพยนตร์</a>
            <a href="<?= url('/admin/theater-seats/add.php') ?>" class="menu-item <?= $page_name === 'เพิ่มที่นั่งโรงภาพยนตร์' ? 'active' : '' ?>">เพิ่มที่นั่งโรงภาพยนตร์</a>
            <a href="<?= url('/admin/theater-seats/list.php') ?>" class="menu-item <?= $page_name === 'รายการที่นั่งโรงภาพยนตร์' ? 'active' : '' ?>">รายการที่นั่งโรงภาพยนตร์</a>

            <span class="menu-title">จัดการการจองที่นั่งโรงภาพยนตร์</span>
            <a href="<?= url('/admin/reserves/request-list.php') ?>" class="menu-item <?= $page_name === 'รายการขอจองที่นั่งโรงภาพยนตร์' ? 'active' : '' ?>">รายการขอจอง</a>
            <a href="<?= url('/admin/reserves/approve-list.php') ?>" class="menu-item <?= $page_name === 'รายการอนุมัติการจองที่นั่งโรงภาพยนตร์' ? 'active' : '' ?>">รายการอนุมัติการจอง</a>
            <a href="<?= url('/admin/reserves/cancel-list.php') ?>" class="menu-item <?= $page_name === 'รายการปฏิเสธการจองที่นั่งโรงภาพยนตร์' ? 'active' : '' ?>">รายการปฏิเสธการจอง</a>

            <span class="menu-title">ข้อมูลส่วนตัว</span>
            <a href="<?= url('/admin/profile/edit.php') ?>" class="menu-item <?= $page_name === 'แก้ไขข้อมูลส่วนตัว' ? 'active' : '' ?>">แก้ไขข้อมูลส่วนตัว</a>
            <a href="<?= url('/admin/profile/edit-pass.php') ?>" class="menu-item <?= $page_name === 'แก้ไขรหัสผ่าน' ? 'active' : '' ?>">แก้ไขรหัสผ่าน</a>
            <a href="<?= url('/auth/logout.php') ?>" <?= clickConfirm('คุณต้องการออกจากระบบหรือไม่') ?> class="menu-item">ออกจากระบบ</a>
            <br><br><br>
        </div>
    </nav>

    <header class="top-bar">
        <div><?= isset($page_name) ? $page_name : '' ?></div>
        <div class="avatar">
            <img src="<?= url($user['profile']) ?>" alt="">
            <span class="name"><?= $user['firstname'] . ' ' . $user['lastname'] ?></span>
            <span> <i>(ผู้ดูแลระบบ)</i></span>
        </div>
    </header>

    <main>
        <?= isset($layout_page) ? $layout_page : '' ?>
        <footer class="">
        </footer>
    </main>
</div>

<?php
$layout_body = ob_get_clean();
$layout_head = '<link rel="stylesheet" href="' . url('/assets/css/layout.css') . '">';
require INC . '/base_layout.php';
