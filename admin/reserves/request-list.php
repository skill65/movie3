<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/reserves/request-list.php';

$action = get('action');
$id = get('id');
switch ($action) {
    case 'approve':
        $qr = $db->query("UPDATE `reserve_action` SET `status`=1 WHERE `reserve_action_id`='$id'");
        if ($qr) {
            setAlert('success', 'อนุมัติการจองสำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด  ไม่สามารถอนุมัติการจองได้');
        }
        redirect($page_path);
        break;
    case 'cancel':
        $qr = $db->query("UPDATE `reserve_action` SET `status`=-1 WHERE `reserve_action_id`='$id'");
        if ($qr) {
            setAlert('success', 'ปฏิเสธการจองสำเร็จ');
        } else {
            setAlert('error', 'เกิดข้อผิดพลาด  ไม่สามารถปฏิเสธการจองได้');
        }
        redirect($page_path);
        break;
}

$re = $db->query("SELECT * FROM `reserve_action`
INNER JOIN `users` ON `users`.`user_id`=`reserve_action`.`user_id`
INNER JOIN `movie_times` ON `movie_times`.`movie_time_id`=`reserve_action`.`movie_time_id`
INNER JOIN `movies` ON `movies`.`movie_id`=`movie_times`.`movie_id`
WHERE `reserve_action`.`status`=0 ");
$items = fetchAll($re);

foreach ($items as &$item) {
    $re_seats = $db->query("SELECT * FROM `reserve_items`
    INNER JOIN `theater_seats` ON `theater_seats`.`theater_seat_id`=`reserve_items`.`theater_seat_id`
    WHERE `reserve_items`.`reserve_action_id`='{$item['reserve_action_id']}'");
    $seats = fetchAll($re_seats);
    $seats_name = [];
    foreach ($seats as $seat) {
        $seats_name[] = $seat['seat_name'];
    }
    $item['seats'] = implode(', ', $seats_name);
    unset($item);
}
ob_start();
?>
<div class="card">
    <div class="card-body">
        <?php showAlert() ?>
        <table>
            <thead>
                <tr>
                    <th>รหัสการจอง</th>
                    <th>ชื่อ</th>
                    <th>นามสกุล</th>
                    <th>อีเมล</th>
                    <th>ชื่อภาพยนตร์</th>
                    <th>วันเวลาฉายภาพยนตร์</th>
                    <th>ที่นั่ง</th>
                    <th>จัดการการจอง</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $item['reserve_action_id'] ?></td>
                        <td><?= $item['firstname'] ?></td>
                        <td><?= $item['lastname'] ?></td>
                        <td><?= $item['email'] ?></td>
                        <td><?= $item['name'] ?></td>
                        <td>
                            <?= $item['start_time'] ?> -
                            <br><?= $item['end_time'] ?>
                        </td>
                        <td><?= $item['seats'] ?></td>
                        <td>
                            <a href="?action=approve&id=<?= $item['reserve_action_id'] ?>" class="btn btn-main btn-sm" <?= clickConfirm("คุณต้องการอนุมัติการขอจองหรือไม่") ?>>อนุมัติ</a>
                            <br>
                            <a href="?action=cancel&id=<?= $item['reserve_action_id'] ?>" class="btn btn-red btn-sm mt-2" <?= clickConfirm("คุณต้องการปฏิเสธการจองหรือไม่") ?>>ปฏิเสธ</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการขอจองที่นั่งโรงภาพยนตร์';

include ROOT . '/admin/layout.php';
