<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$id = get('id');

if (empty($id)) {
    redirect('/admin/movies/list.php');
}
$page_path = "/admin/movies/edit.php?id=$id";

if (!empty($_POST)) {
    if (!empty($_FILES['poster']['name'])) {
        $poster = upload('poster', '/storage/posters');
        if (!$poster) {
            setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแนบรูปภาพได้');
            redirect($page_path);
        }

        $result_poster = $db->query("UPDATE `movies` SET `poster`='{$poster}' WHERE `movie_id`='{$id}'");
        if (!$result_poster) {
            setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแก้ไขรูปภาพได้');
            redirect($page_path);
        }
    }


    $result = $db->query("UPDATE `movies` SET 
    `name`='{$_POST['name']}'
    WHERE `movie_id`='{$id}'");


    if ($result) {
        setAlert('success', 'แก้ไขภาพยนตร์สำเร็จเรียบร้อย');
        redirect($page_path);
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแก้ไขภาพยนตร์ได้');
        redirect($page_path);
    }
}

$re = $db->query("SELECT * FROM `movies` WHERE `movie_id`='{$id}'");
$data = $re->fetch_assoc();
ob_start();
?>

<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post" enctype="multipart/form-data">
            <div class="text-center">
                <label for="poster">
                    <img src="<?= url($data['poster']) ?>" alt="" class="mw-21r mh-21r">
                </label>
            </div>
            <label for="poster">แนบรูปภาพภาพยนตร์</label>
            <input type="file" name="poster" id="poster" class="mb-3">
            <label for="name">ชื่อภาพยนตร์</label>
            <input type="text" name="name" id="name" class="mb-3" value="<?= $data['name'] ?>" required>
            <div class="text-center mt-3">
                <button type="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "แก้ไขภาพยนตร์";
require ROOT . '/admin/layout.php';
