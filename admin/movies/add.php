<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movies/add.php';

if (!empty($_POST)) {
    $poster = upload('poster', '/storage/posters');
    if (!$poster) {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถแนบรูปภาพได้');
        redirect($page_path);
    }

    $result = $db->query("INSERT INTO `movies`(
    `name`,
    `poster`) 
    VALUES (
    '{$_POST['name']}',
    '{$poster}')");

    if ($result) {
        setAlert('success', 'เพิ่มภาพยนตร์สำเร็จเรียบร้อย');
        redirect($page_path);
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถเพิ่มภาพยนตร์ได้');
        redirect($page_path);
    }
}

ob_start();
?>

<div class="card mw-55r mx-auto">
    <div class="card-body">
        <?php showAlert() ?>
        <form method="post" enctype="multipart/form-data">
            <label for="poster">แนบรูปภาพภาพยนตร์</label>
            <input type="file" name="poster" id="poster" class="mb-3" required>
            <label for="name">ชื่อภาพยนตร์</label>
            <input type="text" name="name" id="name" class="mb-3" required>
            <div class="text-center mt-3">
                <button type="submit" class="btn btn-main">
                    บันทึก
                </button>
            </div>
        </form>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "เพิ่มภาพยนตร์";
require ROOT . '/admin/layout.php';
