<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movies/list.php';

$action = get('action');
$id = get('id');

if ($action === 'delete') {
    $re = $db->query("DELETE FROM `movies` WHERE `movie_id`='{$id}'");
    if ($re) {
        setAlert('success', "ลบภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถลบภาพยนตร์ได้");
    }
    redirect($page_path);
}

$reslut = $db->query("SELECT * FROM `movies`");
$items = fetchAll($reslut);

ob_start();
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <?php foreach ($items as $item) : ?>
                <div class="col-3">
                    <div class="card">
                        <img src="<?= url($item['poster']) ?>" alt="" class="card-img">
                        <div class="card-body">
                            <div class="card-title">
                                <?= $item['name'] ?>
                            </div>
                            <i>รหัส: <?= $item['movie_id'] ?></i>
                            <a href="<?= url("/admin/movies/edit.php?id={$item['movie_id']}") ?>" class="btn btn-main btn-sm mt-2 w-100">
                                แก้ไขภาพยนตร์
                            </a>
                            <a href="?action=delete&id=<?= $item['movie_id'] ?>" class="btn btn-red btn-sm mt-2 w-100" <?= clickConfirm("คุณต้องการลบภาพยนตร์หรือไม่") ?>>
                                ลบภาพยนตร์
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php
$layout_page = ob_get_clean();
$page_name = "รายการภาพยนตร์";
require ROOT . '/admin/layout.php';
