<?php
require_once __DIR__ . '/../boot.php';

$page_path = '/auth/login.php';

if (!empty($_POST)) {
    $hash = md5($_POST['password']);
    $result = $db->query("SELECT * FROM `users` WHERE `email`='{$_POST['email']}' AND `password`='{$hash}'");
    if ($result->num_rows == 0) {
        setAlert('error', 'อีเมลหรือรหัสผ่านไม่ถูกต้อง');
        redirect($page_path);
    }

    $user = $result->fetch_assoc();
    switch ($user['status']) {
        case '1':
            # code...
            break;
        
        default:
            setAlert('error', 'บัญชีไม่มีสิทธิ์เข้าสู่ระบบ');
            redirect($page_path);
            break;
    }

    $_SESSION['user_id'] = $user['user_id'];
    switch ($user['user_type']) {
        case 'admin':
            redirect('/admin/index.php');
            break;
        case 'user':
            redirect('/user/index.php');
            break;
    }
}

ob_start();
?>

<div class="auth-container">
    <div class="auth-title">
        ระบบสำรองที่นั่งโรงภาพยนตร์
    </div>
    <div class="card">
        <div class="card-body p-5">
            <div class="auth-header">
                เข้าสู่ระบบ
            </div>
            <?php showAlert() ?> 

            <form method="post">
                <label for="email">อีเมล</label>
                <input type="email" name="email" id="email" class="mb-3" required>
                <label for="password">รหัสผ่าน</label>
                <input type="password" name="password" id="password" class="mb-3" required>

                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-main">
                        เข้าสู่ระบบ
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="text-center mt-3">
        ยังไม่มีบัญชี? <a href="<?= url('/auth/register.php') ?>">สร้างบัญชีของคุณ</a>  
    </div>
</div>

<?php
$layout_body = ob_get_clean();
$layout_head = '<link rel="stylesheet" href="' . url('/assets/css/auth.css') . '">';
require_once INC . '/base_layout.php';
