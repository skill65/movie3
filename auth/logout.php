<?php
require_once __DIR__ . '/../boot.php';
session_destroy();
session_start();
redirect('/auth/login.php');
