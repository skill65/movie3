<?php
require_once __DIR__ . '/../boot.php';

$page_path = '/auth/register.php';

if (!empty($_POST)) {
    $result_email = $db->query("SELECT * FROM `users` WHERE `email`='{$_POST['email']}'");

    if ($result_email->num_rows !== 0) {
        setAlert('error', "มีสมาชิก {$_POST['email']} แล้วไม่สามารถสมัครซ้ำได้");
        redirect($page_path);
    }

    $hash = md5($_POST['password']);
    $result = $db->query("INSERT INTO `users`(
    `firstname`, 
    `lastname`, 
    `profile`, 
    `email`, 
    `password`, 
    `user_type`, 
    `status`) 
    VALUES (
    '{$_POST['firstname']}',
    '{$_POST['lastname']}',
    '/assets/img/user.png',
    '{$_POST['email']}',
    '{$hash}',
    'user',
    1)");

    if ($result) {
        setAlert('success', 'สมัครสมาชิกสำเร็จเรียบร้อย <a href="' . url('/auth/login.php') . '">คลิกที่นี้เพื่อเข้าสู่ระบบ</a>');
        redirect($page_path);
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถสมัครสมาชิกได้');
        redirect($page_path);
    }
}


ob_start();
?>

<div class="auth-container">
    <div class="auth-title">
        ระบบสำรองที่นั่งโรงภาพยนตร์
    </div>
    <div class="card">
        <div class="card-body p-5">
            <div class="auth-header">
                สมัครสมาชิกของคุณ
            </div>
            <?php showAlert() ?> 
            <form method="post">
                <label for="firstname">ชื่อ</label>
                <input type="text" name="firstname" id="firstname" class="mb-3" required>
                <label for="lastname">นามสกุล</label>
                <input type="text" name="lastname" id="lastname" class="mb-3" required>
                <label for="email">อีเมล</label>
                <input type="email" name="email" id="email" class="mb-3" required>
                <label for="password">รหัสผ่าน</label>
                <input type="password" name="password" id="password" class="mb-3" required>

                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-main">
                        สมัครสมาชิก
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="text-center mt-3">
        เป็นสมาชิกแล้ว? <a href="<?= url('/auth/login.php') ?>">ลงชื่อเข้าใช้</a>  
    </div>
</div>

<?php
$layout_body = ob_get_clean();
$layout_head = '<link rel="stylesheet" href="' . url('/assets/css/auth.css') . '">';
require_once INC . '/base_layout.php';
